package app;

import model.Colors;
import model.Seat;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.lang.Math.round;

//TODO
//try/catch
//making more flexible like no static seat in rows

public class Hall extends Colors {
    private int hallSeats;//How many hall has seats
    private int standardSeats;//Variable contains information how much 70% of 100% seats
    private int  vipSeats;//Same is variable higher but 30
    private int howManyUnavailableStandardSeats;//Variable contains number of unavailabe standard seats which are randomized(from 50 of 70 of standard seats).
    private int howManyUnavailableVipSeats;//same mechanism as standard
    private int howManyRows;
    private final char[] LETTERS_TABLE ={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','U','P','R','S','T'};//array which contains letters for collumns
    List<Seat> seats = new ArrayList<>();//list which contains seats(objects)


    public Hall(int hallSeats){//Need to be easy multiply by 0.3 and 0.7 and divide by 2
        for(int i=0;i<hallSeats;i++){//loop for creating seats objects and adding it to list
            double vipDownLimiter = hallSeats/2-(hallSeats*0.3)/2;//to set status for middle seats as vip seats
            double vipUpLimiter =   hallSeats/2+(hallSeats*0.3)/2;//
            Seat seatForSeat = new Seat(i);
            /*
            *We take seat in middle of hall, then 30%/2 minus seats and + 30%/2 are vip seats.
             */
            if(seatForSeat.getNr()>=(int)vipDownLimiter && seatForSeat.getNr()<=(int)vipUpLimiter-1) {
                seatForSeat.setVipSeat(true);
            }
            seats.add(seatForSeat);
        }
        howManyRows=hallSeats/20;
        this.hallSeats = hallSeats;
        standardSeats = (int)(hallSeats * 0.7);//How many standard seat are in hall
        vipSeats = (int)(hallSeats * 0.3);//same but vip
        fillHall(hallSeats);//function that fill hall
    }

    public void fillHall(int hallSeats){
    Random rand = new Random();//Random class constructor
    howManyUnavailableStandardSeats=(int)((double)standardSeats*0.5);//calculating maximal range to random how many seats will be unavailable
    howManyUnavailableVipSeats = (int)((double)vipSeats * 0.5);//same
    int randomStandardSeats = rand.nextInt(0,howManyUnavailableStandardSeats);//finding how many seats will be gone
    int randomVipSeats = rand.nextInt(0,howManyUnavailableVipSeats);
    howManyUnavailableStandardSeats = randomStandardSeats; //result of draw
    howManyUnavailableVipSeats =  randomVipSeats;
    for(int i=0;i<howManyUnavailableVipSeats;i++){//loop for finding and changing status to reserved
        /*
        *Finding mid value and then draw seats that will be reserved limits are set;
        * For example 300 seats -> Finding mid(150) and substact from it 50 up and down of vip seats(30% of all seats) so 150*0.3=90 90/2 = 45
        * so we are on 150 seat -45 +45 is 105-195 are vip seats and random number of seats on this range will be reserved.
        * If seat is already reserved loop will execute again
         */
            int searchAndSelect = (int)rand.nextDouble(((hallSeats/2)-(int)(vipSeats/2)),((hallSeats/2)+(int)(vipSeats/2)));
            if(seats.get(searchAndSelect).getAvailability()!=false) {
                seats.get(searchAndSelect).setAvailability(false);
            }
            else{
                i--;
            }

    }
    for(int i=0;i<howManyUnavailableStandardSeats;i++){
        int searchAndSelect=rand.nextInt(hallSeats);

        if(seats.get(searchAndSelect).isVipSeat()!=true && seats.get(searchAndSelect).getAvailability()!=false){
            seats.get(searchAndSelect).setAvailability(false);
        }
        else{
            --i;
        }
    }

    }

    /*
    *Function for printing app.Hall with row and columns and colorful set of seats.
     */
    public void showHall(){
        //Probably these indexes are unnecessary but with them can be less if operations
        int index = 0;//idex for printing columns letters which task is helping with finding these letters in array
        int listIndex=0;//index for finding seats(objects) in list
        int rowsIndex=0;//index for printing first mark in rows which is number of row

        for(int i=-1;i<howManyRows;i++){

            for(int j=-1;j<LETTERS_TABLE.length;j++){//LETTERS_TABLE is how many are letters for representing columns

                if(i==-1 && j<=18) {//-1 for printing these first row with letters
                    if (i == -1){
                        System.out.print(' ');//space for making easy to read row and column
                    }
                    System.out.print("   "+LETTERS_TABLE[index]);//printing row with letters
                    index++;
                }

                if(j==-1 && i >=0){
                    if(rowsIndex<10) {
                        System.out.print(rowsIndex + "  ");
                    }
                    else{
                        System.out.print(rowsIndex + " ");
                    }
                    rowsIndex++;
                }

                if(i>-1 && j>-1) {
                    if (seats.get(listIndex).getAvailability() == false && seats.get(listIndex).isVipSeat()==false) {//if seat is reserved and isnt vip print number of seat and color it red
                        if(String.valueOf(seats.get(listIndex).getNr()).length()==1) {
                            System.out.print(' ' + ANSI_RED + seats.get(listIndex).getNr() + ANSI_RESET + "   ");//these tree ifs are for spaces which depend on how many digits have number,help with good look
                        }
                        else if(String.valueOf(seats.get(listIndex).getNr()).length()==2){
                            System.out.print(' ' + ANSI_RED + seats.get(listIndex).getNr() + ANSI_RESET + "  ");
                        }
                        else {
                            System.out.print(' ' + ANSI_RED + seats.get(listIndex).getNr() + ANSI_RESET + ' ');
                        }
                    }

                    if (seats.get(listIndex).getAvailability() == true && seats.get(listIndex).isVipSeat()==false) {//color green if is not reserved

                        if(String.valueOf(seats.get(listIndex).getNr()).length()==1) {
                            System.out.print(' ' + ANSI_GREEN + seats.get(listIndex).getNr() + ANSI_RESET + "   ");
                        }
                        else if(String.valueOf(seats.get(listIndex).getNr()).length()==2){
                            System.out.print(' ' + ANSI_GREEN + seats.get(listIndex).getNr() + ANSI_RESET + "  ");
                        }
                        else{
                            System.out.print(' ' + ANSI_GREEN + seats.get(listIndex).getNr() + ANSI_RESET + ' ');
                        }
                    }

                    if (seats.get(listIndex).getAvailability() == true && seats.get(listIndex).isVipSeat()==true) {//color yellow if is vip and not reserved

                        if(String.valueOf(seats.get(listIndex).getNr()).length()==1) {
                            System.out.print(' ' + ANSI_YELLOW + seats.get(listIndex).getNr() + ANSI_RESET + "   ");
                        }
                        else if(String.valueOf(seats.get(listIndex).getNr()).length()==2){
                            System.out.print(' ' + ANSI_YELLOW + seats.get(listIndex).getNr() + ANSI_RESET + "  ");
                        }
                        else{
                            System.out.print(' ' + ANSI_YELLOW + seats.get(listIndex).getNr() + ANSI_RESET + ' ');
                        }
                    }

                    if (seats.get(listIndex).getAvailability() == false && seats.get(listIndex).isVipSeat()==true) {//color red and print number if vip seat is reserved

                        if(String.valueOf(seats.get(listIndex).getNr()).length()==1) {
                            System.out.print(' ' + ANSI_RED + seats.get(listIndex).getNr() + ANSI_RESET + "   ");
                        }
                        else if(String.valueOf(seats.get(listIndex).getNr()).length()==2){
                            System.out.print(' ' + ANSI_RED + seats.get(listIndex).getNr() + ANSI_RESET + "  ");
                        }
                         else{
                            System.out.print(' ' + ANSI_RED + seats.get(listIndex).getNr() + ANSI_RESET + ' ');
                        }
                    }
                        listIndex++;
                }
            }
            System.out.print("\n");
        }
    }

    public String getRowAndColumn(int seatNr){
        char column=' ';
        int switchOperator = seatNr-(seats.get(seatNr).getRow()*20);
        switch(switchOperator){
            case 0:
                column=LETTERS_TABLE[0];
                break;
            case 1:
                column=LETTERS_TABLE[1];
                break;
            case 2:
                column=LETTERS_TABLE[2];
                break;
            case 3:
                column=LETTERS_TABLE[3];
                break;
            case 4:
                column=LETTERS_TABLE[4];
                break;
            case 5:
                column=LETTERS_TABLE[5];
                break;
            case 6:
                column=LETTERS_TABLE[6];
                break;
            case 7:
                column=LETTERS_TABLE[7];
                break;
            case 8:
                column=LETTERS_TABLE[8];
                break;
            case 9:
                column=LETTERS_TABLE[9];
                break;
            case 10:
                column=LETTERS_TABLE[10];
                break;
            case 11:
                column=LETTERS_TABLE[11];
                break;
            case 12:
                column=LETTERS_TABLE[12];
                break;
            case 13:
                column=LETTERS_TABLE[13];
                break;
            case 14:
                column=LETTERS_TABLE[14];
                break;
            case 15:
                column=LETTERS_TABLE[15];
                break;
            case 16:
                column=LETTERS_TABLE[16];
                break;
            case 17:
                column=LETTERS_TABLE[17];
                break;
            case 18:
                column=LETTERS_TABLE[18];
                break;
            case 19:
                column=LETTERS_TABLE[19];
                break;
            default:
                System.out.println("B)");
                break;
        }
        return "Row:" + seats.get(seatNr).getRow() + " Column:" + column;
    }

    public String HowManyAvailabeSeats(){
        return "Ilosc wolnych zwyklych:"+(standardSeats-howManyUnavailableStandardSeats)+" Ilosc wolnych vip: "+(vipSeats-howManyUnavailableVipSeats);
    }

    public int getStandardSeats() {
        return standardSeats;
    }

    public void setStandardSeats(int standardSeats) {
        this.standardSeats = standardSeats;
    }

    public int getVipSeats() {
        return vipSeats;
    }

    public void setVipSeats(int vipSeats) {
        this.vipSeats = vipSeats;
    }

    public int getHallSeats() {
        return hallSeats;
    }

    public void setHallSeats(int hallSeats) {
        this.hallSeats = hallSeats;
    }

    public int getHowManyUnavailableStandardSeats() {
        return howManyUnavailableStandardSeats;
    }

    public void setHowManyUnavailableStandardSeats(int howManyUnavailableStandardSeats) {
        this.howManyUnavailableStandardSeats = howManyUnavailableStandardSeats;
    }

    public int getHowManyUnavailableVipSeats() {
        return howManyUnavailableVipSeats;
    }

    public void setHowManyUnavailableVipSeats(int howManyUnavailableVipSeats) {
        this.howManyUnavailableVipSeats = howManyUnavailableVipSeats;
    }


}
