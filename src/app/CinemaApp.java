package app;

import java.io.IOException;
import java.sql.SQLOutput;
import java.util.Arrays;

public class CinemaApp {
    public static void main(String[] args) throws IOException {

        final String APP_NAME = "Cinema v0.1";
        final String FIRST_CHEF = "Jakub Fik 34281";
        final String SECOND_CHEF = "Krzysztof Książek 34301";

        System.out.println("----------------------------------------");
        System.out.println("Application Name: " + APP_NAME);
        System.out.println("Student one: " + FIRST_CHEF);
        System.out.println("Student two: " + SECOND_CHEF);
        System.out.println("----------------------------------------");
        CinemaSystem cinemaSystem = new CinemaSystem();
        cinemaSystem.controlLoop();





    }
}
