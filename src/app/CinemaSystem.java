package app;

import io.CsvReader;
import io.DataReader;
import model.Client;
import model.Shows;
import model.Ticket;

import java.io.IOException;

public class CinemaSystem {
    private static final String EXIT = "n";
    private static final String BUY_TICKET = "y";
    private static final String MONDAY = "1";
    private static final String TUESDAY = "2";
    private static final String WEDNESDAY = "3";
    private static final String THURSDAY = "4";
    private static final String FRIDAY = "5";
    private static final String SATURDAY = "6";
    private static final String SUNDAY = "7";
    private static final int MAX_TICKETS = 10;
    private Client[] clients;
    private Ticket[] tickets;
    private DataReader dataReader = new DataReader();
    private Hall bigHall = new Hall(300);
    private Shows show = new Shows();
//    public CsvReader csvReader = new CsvReader();

    public void controlLoop() throws IOException {

        /**
        *Simple menu system for making a choice if we want to buy a ticket or not,negative answer will
        *lead us to exit,otherwise we will be able to buy a specific ticket for a show in specific day
        *with a list of shows that day
         */
        String purchaseDecision;
        do {
            System.out.print("Do you want to buy a ticket? (y/n): ");
            purchaseDecision = dataReader.getString();

            switch (purchaseDecision) {
                /** Case that realize the ticket purchase process */
                case BUY_TICKET -> {

                    /** Print days of week */
                    printDayOfWeek();

                    /** User can select the day of the week on which he wants to buy a ticket */

                        int dayOfWeek = dataReader.getInt();

                    /** Print  repertoire based on select day by user */
                    viewCinemaRepertoire(dayOfWeek);

                    /** User select how many tickets he want to buy ?*/
                    System.out.print("How many tickets you want to buy (Max 10): ");

                    int howManyTickets = dataReader.getInt();
                    if (howManyTickets > MAX_TICKETS) {
                        throw new IllegalArgumentException("Soo many tickets!");
                    }

                    /** User select movie based on id of movie */
                    System.out.print("Select id off repertoire: ");
                    String repertoireId = dataReader.getString();

                    /** Get clients data (name and age) and whether the required age is correct  */
                    clients = getDataFromClient(howManyTickets,show.getShow(repertoireId));

                    /** Ordering Ticket */
                    orderingTicket(howManyTickets,clients,show.getShow(repertoireId),String.valueOf(repertoireId));

                }
                /** End of program */
                case EXIT -> System.out.println("Goodbye");

                /** Incorrect option*/
                default -> System.out.println("Invalid option");
            }
        } while(!purchaseDecision.equals(EXIT));

    }

    /**
     *
     * @param howManyTickets How Many Tickets Client Want To Buy
     * @param show Movie Show
     * @return Name and Age off Clients
     */
    private Client[] getDataFromClient(int howManyTickets,Shows show) {
        Client[] clients = new Client[howManyTickets];
        int age = 0;
        String name = null;
        for (int i = 0; i < clients.length; i++) {
            System.out.print("Enter name of " +  (i + 1) + ". client: ");
            name = dataReader.getString();
            System.out.print("Enter age: ");
            age = dataReader.getInt();
            if (age < Integer.parseInt(show.getRequiredAge())){
                // TODO arthemtic exc or other?
                throw new ArithmeticException("You are to young!");
            } else {
                clients[i] = new Client(name, age);
            }
        }

        return clients;
    }

    /**
     *
     * @param howManyTickets How Many Tickets
     * @param clients Clients data
     * @param show Movie Show
     * @param repertoireId ID off Select repertoire
     * @throws IOException
     * Call the method that create the tickets
     */
    private void orderingTicket(int howManyTickets, Client[] clients, Shows show, String repertoireId) throws IOException {
        bigHall.showHall();
        int [] seats = new int[howManyTickets];

        for (int i = 0; i < howManyTickets; i++) {
            System.out.println("Select number of seat: ");
             seats[i] = dataReader.getInt();

            if (bigHall.seats.get(seats[i]).getAvailability() == false) {
                System.err.println("Seat is already reserved!");
                i--;

            } else if(bigHall.seats.get(seats[i]).getAvailability() == true) {
                if(bigHall.seats.get(seats[i]).isVipSeat() == true){
                    bigHall.seats.get(seats[i]).setAvailability(false);
                    clients[i].setVip(true);
                    clients[i].setSeat(bigHall.getRowAndColumn(seats[i]) + "|" + seats[i]);

                } else if (bigHall.seats.get(seats[i]).isVipSeat() == false) {
                    bigHall.seats.get(seats[i]).setAvailability(false);
                    clients[i].setVip(false);
                    clients[i].setSeat(bigHall.getRowAndColumn(seats[i]) + "|" + seats[i]);
                }
                System.out.println(bigHall.getRowAndColumn(seats[i]));
            }
        }
        tickets = TicketSystem.createTicket(repertoireId,show,clients,howManyTickets);
        /** Make txt/pdf file with tickets*/
        new PrintTicket(tickets);

    }

    /**
     * Method that displays the repertoire for a given day
     * @param dayOfWeek Day off Select Week
     */
    private void viewCinemaRepertoire(int dayOfWeek) {
        int day = dayOutOffRange(dayOfWeek);
        String [] daysOfWeek = new String[]{"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"}; // 1
        show.viewRepertoire(daysOfWeek[day - 1]);
    }

    /**
     * A method to protect against entering an incorrect range of days
     * When user give number off day -> 8 that method return Monday etc.
     * @param dayOfWeek Day off Select Week
     * @return Day off Week
     */
    private int dayOutOffRange(int dayOfWeek) {
        if( dayOfWeek < 7 ) {
            return dayOfWeek;
        } else {
            while( dayOfWeek > 7 ) {
                dayOfWeek -= 7;
            }
            return dayOfWeek;
        }
    }

    /**
     * Method thad display days off week
     */
    private void printDayOfWeek() {
        System.out.println(MONDAY + "-> Monday");
        System.out.println(TUESDAY + "-> Tuesday");
        System.out.println(WEDNESDAY + "-> Wednesday");
        System.out.println(THURSDAY + "-> Thursday");
        System.out.println(FRIDAY + "-> Friday");
        System.out.println(SATURDAY + "-> Saturday");
        System.out.println(SUNDAY + "-> Sunday");
        System.out.print("Choose a day and display schedule: ");
    }
}
