package app;

import model.Client;
import model.Shows;
import model.Ticket;

public class TicketSystem {


    public static Ticket [] createTicket(String repertoireId, Shows show, Client[] clients, int howManyTickets) {
        /** Arrays off Tickets */
        Ticket [] tickets = new Ticket[howManyTickets];
        double price = 0;
        for (int i = 0; i < howManyTickets; i++) {
            /** Calculate price of ticket */
            price = getPriceTicket(show, clients[i]);
            tickets[i] = new Ticket(repertoireId,show,clients[i],price);
        }
        return tickets;
    }

    /**
     * Price is depend on length off movie and seat (is Vip or not)
     * @param show Movie Show
     * @param client Client
     * @return Price off Ticket
     */
    private static double getPriceTicket(Shows show, Client client) {
        double price = 0;

            if(client.isVip()){
                price = (Integer.parseInt(show.getDuration()) * 0.2) + 5.0;

            } else {
                price = (Integer.parseInt(show.getDuration()) * 0.2);

            }

        return  Math.round(price * 100.0) / 100.0;
    }
}
