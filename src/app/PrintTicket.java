package app;

import model.Ticket;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class PrintTicket {
    /** Saving tickets to txt file */
    public PrintTicket(Ticket[] tickets) throws IOException {
        String fileName = "ticket.txt";
        File file = new File(fileName);
        FileWriter fileWriter = new FileWriter(fileName);
        BufferedWriter writer = new BufferedWriter(fileWriter);
        boolean fileExists = file.exists();
        if (!fileExists) {
            try {
                fileExists = file.createNewFile();


            }catch (IOException e ){
                System.err.println("Error!");
            }
        }
        if (fileExists)

            for (int i = 0; i < tickets.length; i++) {

                writer.write(tickets[i].getMovieShow().getTitle() + " " + tickets[i].getMovieShow().getDubbingOrSubtitle() + "\n" +
                        "Name / age: " + tickets[i].getClient().getName() + " / " + tickets[i].getClient().getAge() + "\n" +
                        "Price: " + tickets[i].getPrice() + " VIP seat? " + tickets[i].getClient().isVip() +  "\n"
                                + tickets[i].getClient().getSeat() + "\n" +
                         "ID of Repertoire: " + tickets[i].getId() + "\n" + "Enjoy the show!" + "\n\n") ;

                writer.newLine();

            }

        writer.close();


    }
}
