package model;

import io.CsvReader;

import java.util.ArrayList;
import java.util.List;

/**
 * Class which contains main information about show and objects of this class
 * will be injected to a list.Information are placed in csv.
 */
public class Shows extends CsvReader {
    private String id;
    private String title;
    private String numberOfHall;
    private String startTime;
    private String duration;
    private String dayOfWeek;
    private String requiredAge;
    private String dubbingOrSubtitle;

    public ArrayList<Shows> showsList = new ArrayList();
/*
Przejście przez liste z repertuarami i jesli bedzie zgadzał się dzień tygodnia to git.
1. Utworzenie tablicy 7 elementowej klasy model.Shows
2. Pola klasy Show (stringi) utowrza jednego wielkiego stringa ktory bedzie wrzucony do listy dla kolejnego show tego dnia
    zmienne te beda nadpisane i zapisane odpowiednio do stringa i dodane do kolejnego show tego dnia do listy
    3. shows tablica w zaleznosci od dnia
 */


    public Shows() {
    }

    public Shows(String id, String title, String numberOfHall, String startTime, String duration, String dayOfWeek, String requiredAge, String dubbingOrSubtitle) {
        this.id = id;
        this.title = title;
        this.numberOfHall = numberOfHall;
        this.startTime = startTime;
        this.duration = duration;
        this.dayOfWeek = dayOfWeek;
        this.requiredAge = requiredAge;
        this.dubbingOrSubtitle = dubbingOrSubtitle;
    }

    public void viewRepertoire(String dayOfWeek) {

        for (List<String> record : records) {
            if (record.subList(5, 6).contains(dayOfWeek)) {
                showsList.add(new Shows(record.get(0), record.get(1), record.get(2), record.get(3), record.get(4), record.get(5),
                        record.get(6), record.get(7)));
            }

        }
        System.out.println("ID|Title|Hall number|Start|Length (min)|Day|Min Age|Dub/Sub");
        for (Shows shows : showsList) {
            System.out.println(shows);
        }
    }

    public Shows getShow(String id) {
        int index = -1;
        for (int i = 0; i < showsList.size(); i++) {
            if (showsList.get(i).getId().equals(id)) {
                index = i;

            }
        }
        try {
            System.out.println(showsList.get(index));
        } catch (IndexOutOfBoundsException e) {
            System.err.println("Wrong id!");
        }
        return showsList.get(index);
    }



    @Override
    public String toString() {
        return id + "|" + title + "|" + numberOfHall + "|" + startTime + "|" + duration + "|" + dayOfWeek + "|"
                + requiredAge + "|" + dubbingOrSubtitle;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getNumberOfHall() {
        return numberOfHall;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getDuration() {
        return duration;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public String getRequiredAge() {
        return requiredAge;
    }

    public String getDubbingOrSubtitle() {
        return dubbingOrSubtitle;
    }

    public ArrayList<Shows> getShowsList() {
        return showsList;
    }
}
