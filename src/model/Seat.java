package model;

public class Seat {
   private Boolean availability=true;
   private int nr;
   private int row;
   private boolean vipSeat;
    public Seat(int nr){
        this.nr=nr;
        row =(int)(nr/20);
    }

    public Boolean getAvailability() {
        return availability;
    }

    public void setAvailability(Boolean availability) {
        this.availability = availability;
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public boolean isVipSeat() {
        return vipSeat;
    }

    public void setVipSeat(boolean vipSeat) {
        this.vipSeat = vipSeat;
    }

    @Override
    public String toString() {
        return "Number: " + nr + " Row " + row + " isVip? " + vipSeat;
    }
}
