package model;

public class Client {
    private String name;
    private int age;
    private boolean isVip;
    private String seat;

    public Client(String name, int age) {
        this.name = name;
        this.age = age;

    }


    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public boolean isVip() {
        return isVip;
    }

    public void setVip(boolean vip) {
        isVip = vip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "model.Client: " + name + ", age: " + age;
    }
}
