package model;

import model.Client;

public class Ticket {

    private String id;
    private Shows movieShow;
    private Client client;
    private double price;


    public Ticket(String id, Shows movieShow, Client client, double price) {
        this.id = id;
        this.movieShow = movieShow;
        this.client = client;
        this.price = price;

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Shows getMovieShow() {
        return movieShow;
    }

    public void setMovieShow(Shows movieShow) {
        this.movieShow = movieShow;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


}
