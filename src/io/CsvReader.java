package io;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CsvReader{
    private String csvPath = "C:\\Users\\krzys\\Desktop\\JAVA\\project_04_34281\\repertuar_kino.csv";
    public static final String SEPARATOR = ",";
    File file = new File(csvPath);
    public List<List<String>> records = new ArrayList<>();

    public CsvReader() {
        separating();

    }
    FileReader fileReader;{
        try {
            fileReader = new FileReader(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    BufferedReader bufferedReader = new BufferedReader(fileReader);


    /**
     * Saving csv lines first to string then splitting and setting to an array(needed) after this saving it to arraylist
     *     * with using method array as list(converter or something)
     */
   public void separating() {
       String line=null;
       try {
           bufferedReader.readLine();
       } catch (IOException e) {
           e.printStackTrace();
       }
       while (true) {
           try {
               if (!((line = bufferedReader.readLine()) != null)) break;
           } catch (IOException e) {
               e.printStackTrace();
           }
           String[] afterSplitting = line.split(SEPARATOR);
           records.add(Arrays.asList(afterSplitting));
       }
   }

    public void checkFileExistence(){
        Boolean fileExistenceChecker;
        fileExistenceChecker = file.exists();
        if(fileExistenceChecker == true){//Instead of IF -> Try catch
            System.out.println("File exists!");
        }else {
            System.out.println("File don't exists (FIND FILE 'ShowData' AND PASTE FILE PATH TO csvPath variable.Goodbye!");
        }
    }






}
