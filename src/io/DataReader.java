package io;

import java.util.Scanner;

public class DataReader {
    /**
     * DataReader class responsible for handling input data
     */
    private final Scanner sc;
    public DataReader() {
        this.sc = new Scanner(System.in);
    }

    public String getString() {
        return this.sc.nextLine();
    }

    public int getInt(){
        int number = this.sc.nextInt();
        this.sc.nextLine();
        return number;

    }
    public void close() {
        this.sc.close();
    }
}
